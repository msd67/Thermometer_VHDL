function w=crc()

a = input('please enter hex number: ');

b = hexToBinaryVector(a);
b = fliplr(b);

reg = zeros(1,8);

for j=1:64
    if j<=length(b)
        bit_temp = bitxor(reg(8), b(j));
    else
       bit_temp = bitxor(reg(8), 0); 
    end
    reg(8) = reg(7);
    reg(7) = reg(6);
    reg(6) = bitxor(reg(5), bit_temp);
    reg(5) = bitxor(reg(4), bit_temp);
    reg(4) = reg(3);
    reg(3) = reg(2);
    reg(2) = reg(1);
    reg(1) = bit_temp;
end

w = reg;