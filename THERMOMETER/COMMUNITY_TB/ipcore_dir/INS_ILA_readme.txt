The following files were generated for 'INS_ILA' in directory
C:\Users\masoud\Documents\FPGA_T\Personal Training\Thermometer3\THERMOMETER\COMMUNITY_TB\ipcore_dir\

XCO file generator:
   Generate an XCO file for compatibility with legacy flows.

   * INS_ILA.xco

Creates an implementation netlist:
   Creates an implementation netlist for the IP.

   * INS_ILA.cdc
   * INS_ILA.constraints/INS_ILA.ucf
   * INS_ILA.constraints/INS_ILA.xdc
   * INS_ILA.ncf
   * INS_ILA.ngc
   * INS_ILA.ucf
   * INS_ILA.vhd
   * INS_ILA.vho
   * INS_ILA.xdc
   * INS_ILA_xmdf.tcl

Creates an HDL instantiation template:
   Creates an HDL instantiation template for the IP.

   * INS_ILA.vho

IP Symbol Generator:
   Generate an IP symbol based on the current project options'.

   * INS_ILA.asy

SYM file generator:
   Generate a SYM file for compatibility with legacy flows

   * INS_ILA.sym

Generate ISE subproject:
   Create an ISE subproject for use when including this core in ISE designs

   * INS_ILA.gise
   * INS_ILA.xise
   * _xmsgs/pn_parser.xmsgs

Deliver Readme:
   Readme file for the IP.

   * INS_ILA_readme.txt

Generate FLIST file:
   Text file listing all of the output files produced when a customized core was
   generated in the CORE Generator.

   * INS_ILA_flist.txt

Please see the Xilinx CORE Generator online help for further details on
generated files and how to use them.

