The following files were generated for 'INS_ICON' in directory
C:\Users\masoud\Documents\FPGA_T\Personal Training\Thermometer3\THERMOMETER\COMMUNITY_TB\ipcore_dir\

XCO file generator:
   Generate an XCO file for compatibility with legacy flows.

   * INS_ICON.xco

Creates an implementation netlist:
   Creates an implementation netlist for the IP.

   * INS_ICON.constraints/INS_ICON.ucf
   * INS_ICON.constraints/INS_ICON.xdc
   * INS_ICON.ngc
   * INS_ICON.ucf
   * INS_ICON.vhd
   * INS_ICON.vho
   * INS_ICON.xdc
   * INS_ICON_xmdf.tcl

IP Symbol Generator:
   Generate an IP symbol based on the current project options'.

   * INS_ICON.asy

SYM file generator:
   Generate a SYM file for compatibility with legacy flows

   * INS_ICON.sym

Generate ISE subproject:
   Create an ISE subproject for use when including this core in ISE designs

   * INS_ICON.gise
   * INS_ICON.xise
   * _xmsgs/pn_parser.xmsgs

Deliver Readme:
   Readme file for the IP.

   * INS_ICON_readme.txt

Generate FLIST file:
   Text file listing all of the output files produced when a customized core was
   generated in the CORE Generator.

   * INS_ICON_flist.txt

Please see the Xilinx CORE Generator online help for further details on
generated files and how to use them.

