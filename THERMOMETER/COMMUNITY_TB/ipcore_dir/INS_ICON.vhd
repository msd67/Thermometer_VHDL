-------------------------------------------------------------------------------
-- Copyright (c) 2018 Xilinx, Inc.
-- All Rights Reserved
-------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor     : Xilinx
-- \   \   \/     Version    : 14.7
--  \   \         Application: XILINX CORE Generator
--  /   /         Filename   : INS_ICON.vhd
-- /___/   /\     Timestamp  : Mon Apr 16 13:09:54 Iran Daylight Time 2018
-- \   \  /  \
--  \___\/\___\
--
-- Design Name: VHDL Synthesis Wrapper
-------------------------------------------------------------------------------
-- This wrapper is used to integrate with Project Navigator and PlanAhead

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
ENTITY INS_ICON IS
  port (
    CONTROL0: inout std_logic_vector(35 downto 0));
END INS_ICON;

ARCHITECTURE INS_ICON_a OF INS_ICON IS
BEGIN

END INS_ICON_a;
