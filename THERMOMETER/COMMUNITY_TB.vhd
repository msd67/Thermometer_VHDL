LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;


-- CLOCK RATE = 50MHz, EQUAL CLOCK PERIOD = 20 NS
-- COMMUNICATION WITH THERMOMETER 
-- INFORMATION IS SENT TO/FROM THE DS18B20 OVER A 1-WIRE INTERFACE

ENTITY COMMUNITY_TB	IS
	PORT(
		CLOCK_IN		: IN		STD_LOGIC;
		BUTTON			: IN		STD_LOGIC;
		SCPD			: INOUT		STD_LOGIC;
		LED				: OUT		STD_LOGIC
	);
END COMMUNITY_TB;

ARCHITECTURE BEHAVIORAL	OF COMMUNITY_TB	IS

	component INS_ICON
	  PORT (
		CONTROL0 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0));
	end component;
	
	component INS_ILA
	  PORT (
		CONTROL : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
		CLK : IN STD_LOGIC;
		DATA : IN STD_LOGIC_VECTOR(143 DOWNTO 0);
		TRIG0 : IN STD_LOGIC_VECTOR(0 TO 0));
	end component;
	
	SIGNAL CONTROL0		: STD_LOGIC_VECTOR(35 DOWNTO 0)		:= (OTHERS=>'0');
	SIGNAL TRIG0		: STD_LOGIC_VECTOR(00 DOWNTO 0)		:= (OTHERS=>'0');

	component INS_CLOCK
	port
	 (-- Clock in ports
	  CLK_IN1           : in     std_logic;
	  -- Clock out ports
	  CLK_OUT1          : out    std_logic
	 );
	end component;
	
	SIGNAL CLOCK		: STD_LOGIC					:= '0';
	
	-- STATE MACHIN COMMANDS
	CONSTANT READ_ROM	: UNSIGNED(03 DOWNTO 0)		:= TO_UNSIGNED(0, 4);
	CONSTANT MATCH_ROM	: UNSIGNED(03 DOWNTO 0)		:= TO_UNSIGNED(1, 4);
	CONSTANT SEARCH_ROM	: UNSIGNED(03 DOWNTO 0)		:= TO_UNSIGNED(2, 4);
	CONSTANT SKIP_ROM	: UNSIGNED(03 DOWNTO 0)		:= TO_UNSIGNED(3, 4);
	CONSTANT ALARM_SRCH	: UNSIGNED(03 DOWNTO 0)		:= TO_UNSIGNED(4, 4);
	CONSTANT WR_SCPD	: UNSIGNED(03 DOWNTO 0)		:= TO_UNSIGNED(5, 4);
	CONSTANT RD_SCPD	: UNSIGNED(03 DOWNTO 0)		:= TO_UNSIGNED(6, 4);
	CONSTANT CPY_SCPD	: UNSIGNED(03 DOWNTO 0)		:= TO_UNSIGNED(7, 4);
	CONSTANT CNVRT_T	: UNSIGNED(03 DOWNTO 0)		:= TO_UNSIGNED(8, 4);
	CONSTANT RECALL		: UNSIGNED(03 DOWNTO 0)		:= TO_UNSIGNED(9, 4);
	CONSTANT RPS		: UNSIGNED(03 DOWNTO 0)		:= TO_UNSIGNED(10, 4);
	CONSTANT RST_PLS	: UNSIGNED(03 DOWNTO 0)		:= TO_UNSIGNED(11, 4);
	CONSTANT READ_CDE	: UNSIGNED(03 DOWNTO 0)		:= TO_UNSIGNED(12, 4);

	SIGNAL BUTTON_INT	: STD_LOGIC					:= '0';
	SIGNAL BTN_PRV		: STD_LOGIC					:= '0';
	SIGNAL BTN			: STD_LOGIC					:= '0';
	SIGNAL READY		: STD_LOGIC					:= '1';
	SIGNAL READY_PRV	: STD_LOGIC					:= '0';
	SIGNAL STP_MIN		: STD_LOGIC					:= '0';
	SIGNAL STP_MIN1		: STD_LOGIC					:= '0';
	SIGNAL JST_MIN		: STD_LOGIC					:= '0';
	SIGNAL JST_MIN1		: STD_LOGIC					:= '0';
	SIGNAL JST_MIN2		: STD_LOGIC					:= '0';
	SIGNAL CONF_VLU		: STD_LOGIC					:= '0';
	SIGNAL WAIT_TIME	: STD_LOGIC					:= '0';
	SIGNAL NXT_VLU		: STD_LOGIC					:= '0';
	SIGNAL LED_INT		: STD_LOGIC					:= '1';
	SIGNAL FRST_DATA	: STD_LOGIC					:= '0';
	SIGNAL CORREC		: STD_LOGIC					:= '0';

	SIGNAL COUNT		: UNSIGNED(25 DOWNTO 0)		:= (OTHERS=>'0');
	SIGNAL COMMAND		: UNSIGNED(03 DOWNTO 0)		:= (OTHERS=>'1');
	SIGNAL SCPD_WRT		: UNSIGNED(07 DOWNTO 0)		:= (OTHERS=>'0');
	SIGNAL DATA_THRMO	: UNSIGNED(71 DOWNTO 0)		:= (OTHERS=>'0');
	SIGNAL DATA_THRMO1	: UNSIGNED(71 DOWNTO 0)		:= (OTHERS=>'0');
	SIGNAL DATA			: UNSIGNED(71 DOWNTO 0)		:= (OTHERS=>'0');
	SIGNAL CODE_ADD		: UNSIGNED(00 DOWNTO 0)		:= (OTHERS=>'0');
	
	TYPE CODE_LEVEL	IS (LVL0, LVL1, LVL2, LVL3, LVL4, LVL5, LVL6, LVL7, LVL8, LVL9, LVL10, LVL11, LVL12, NOTING);
	SIGNAL CDE_LVL		: CODE_LEVEL				:= NOTING;

BEGIN

	LED			<= LED_INT;

	PROCESS(CLOCK)
	BEGIN
	
		IF RISING_EDGE(CLOCK)	THEN
		
			COUNT			<= COUNT + 1;
		
			BUTTON_INT		<= NOT BUTTON;
			BTN_PRV			<= BTN;
			
			READY_PRV		<= READY;
			
			TRIG0(0)		<= '0';
			
			IF BTN='1'	AND BTN_PRV='0'	THEN
				CDE_LVL			<= LVL0;
				COUNT			<= (OTHERS=>'0');
				LED_INT			<= NOT LED_INT;
			END IF;
			
			COMMAND			<= (OTHERS=>'1');
			STP_MIN1		<= '1';
			STP_MIN			<= STP_MIN1;
			
			IF READY='1'	AND STP_MIN='1'	THEN
				CASE CDE_LVL	IS
					WHEN	LVL0	=>
						COMMAND		<= RST_PLS;
						CDE_LVL		<= LVL1;
						STP_MIN		<= '0';
						STP_MIN1	<= '0';
						
					
					WHEN	LVL1	=>
						COMMAND		<= READ_ROM;
						CDE_LVL		<= NOTING;
						JST_MIN		<= '1';
						STP_MIN		<= '0';
						STP_MIN1	<= '0';
						JST_MIN1	<= '1';
					
					WHEN	LVL2	=>
						COMMAND		<= RST_PLS;
						CDE_LVL		<= LVL3;
						STP_MIN		<= '0';
						STP_MIN1	<= '0';
					
					WHEN	LVL3	=>
						COMMAND		<= SKIP_ROM;
						CDE_LVL		<= LVL4;
						STP_MIN		<= '0';
						STP_MIN1	<= '0';
					
					WHEN	LVL4	=>
						COMMAND		<= WR_SCPD;
						CDE_LVL		<= NOTING;
						STP_MIN		<= '0';
						STP_MIN1	<= '0';
						SCPD_WRT	<= X"1E";
						CONF_VLU	<= '1';
					
					WHEN	LVL5	=>
						COMMAND		<= RST_PLS;
						STP_MIN		<= '0';
						STP_MIN1	<= '0';
						CDE_LVL		<= LVL6;
					
					WHEN	LVL6	=>
						COMMAND		<= SKIP_ROM;
						CDE_LVL		<= LVL7;
						STP_MIN		<= '0';
						STP_MIN1	<= '0';
					
					WHEN	LVL7	=>
						COMMAND		<= CNVRT_T;
						CDE_LVL		<= NOTING;
						STP_MIN		<= '0';
						STP_MIN1	<= '0';
						WAIT_TIME	<= '1';
						COUNT		<= (OTHERS=>'0');
					
					WHEN	LVL8	=>
						COMMAND		<= RST_PLS;
						CDE_LVL		<= LVL9;
						STP_MIN		<= '0';
						STP_MIN1	<= '0';
					
					WHEN	LVL9	=>
						COMMAND		<= SKIP_ROM;
						CDE_LVL		<= LVL10;
						STP_MIN		<= '0';
						STP_MIN1	<= '0';
					
					WHEN	LVL10	=>
						COMMAND		<= RD_SCPD;
						CDE_LVL		<= NOTING;
						STP_MIN		<= '0';
						STP_MIN1	<= '0';
						FRST_DATA	<= '0';
						JST_MIN1	<= '1';
						JST_MIN		<= '1';
						CORREC		<= '1';
						COUNT		<= (OTHERS=>'0');
					
					WHEN	LVL11	=>
						COMMAND		<= RST_PLS;
						CDE_LVL		<= NOTING;
						STP_MIN		<= '0';
						STP_MIN1	<= '0';
						CORREC		<= '0';
						TRIG0(0)		<= '1';
					
					WHEN	LVL12	=>
					WHEN	NOTING	=>
				
				END CASE;
			END IF;
			
			IF READY='1'	AND READY_PRV='0'	THEN
				DATA_THRMO		<= DATA;
				FRST_DATA		<= NOT FRST_DATA;
			END IF;
			
			IF FRST_DATA='1'	AND JST_MIN1='1'	THEN
				DATA_THRMO1		<= DATA_THRMO;
				JST_MIN1		<= '0';
				JST_MIN2		<= '1';
			END IF;
			
			IF DATA_THRMO(71 DOWNTO 68)=X"B"	AND JST_MIN='1'	AND JST_MIN2='1'	THEN
				CDE_LVL			<= LVL2;
				JST_MIN			<= '0';
				JST_MIN2		<= '0';
				IF CORREC='1'	THEN
					CDE_LVL			<= LVL11;
				END IF;
			END IF;
			
			IF CORREC='1'	THEN
				IF COUNT=TO_UNSIGNED(185000, 26)	THEN
					
				END IF;
			END IF;
			
			IF CONF_VLU='1'	THEN
				SCPD_WRT		<= X"0A";
				NXT_VLU			<= '1';
				CONF_VLU		<= '0';
			END IF;
			IF NXT_VLU='1'	THEN
				SCPD_WRT		<= X"7F";
				NXT_VLU			<= '0';
				CDE_LVL			<= LVL5;
			END IF;
			
			IF WAIT_TIME='1'	THEN
				IF COUNT=TO_UNSIGNED(37500000, 26)	THEN
					WAIT_TIME		<= '0';
					CDE_LVL			<= LVL8;
				END IF;
			END IF;
		
		END IF;	-- RISING EDGE
	
	END PROCESS;

	INS_THERMO:		ENTITY WORK.THERMO(BEHAVIORAL)
	PORT MAP(
		CLOCK, COMMAND, CODE_ADD, SCPD_WRT, SCPD, READY, DATA
	);
	
	INS_DEBOUNCE:	ENTITY WORK.DEBOUNCE(BEHAVIORAL)
	PORT MAP(
		CLOCK, '0', BUTTON_INT, OPEN, BTN
	);
	
	INS_ICON0 : INS_ICON
	port map (
	CONTROL0 => CONTROL0);
	
	INS_ILA0 : INS_ILA
	port map (
	CONTROL => CONTROL0,
	CLK => CLOCK,
	DATA => STD_LOGIC_VECTOR(DATA_THRMO & DATA_THRMO1),
	TRIG0 => TRIG0);
	
	INS_CLOCK0 : INS_CLOCK
	port map
	(-- Clock in ports
	CLK_IN1 => CLOCK_IN,
	-- Clock out ports
	CLK_OUT1 => CLOCK);
	
END BEHAVIORAL;