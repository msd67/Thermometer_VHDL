LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

-- CLOCK RATE = 50MHz, EQUAL CLOCK PERIOD = 20 NS
-- TEMPERATURE COMMUNICATION TEST

ENTITY THRM_TB	IS
	PORT(
		CLOCK_IN	: IN	STD_LOGIC;
		BUTTON		: IN	STD_LOGIC;
		LED			: OUT	STD_LOGIC;
		Seven_Segment	: OUT	UNSIGNED(07 DOWNTO 0);
		TPTR		: INOUT	STD_LOGIC	-- TEMPERATURE
	);
END THRM_TB;

ARCHITECTURE BEHAVIORAL	OF THRM_TB	IS

	component CLOCK_BUF
	port
	 (-- Clock in ports
	  CLK_IN1           : in     std_logic;
	  -- Clock out ports
	  CLK_OUT1          : out    std_logic
	 );
	end component;

	component ICON
	  PORT (
		CONTROL0 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0));

	end component;
	
	component ILA
	  PORT (
		CONTROL : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
		CLK : IN STD_LOGIC;
		DATA : IN STD_LOGIC_VECTOR(71 DOWNTO 0);
		TRIG0 : IN STD_LOGIC_VECTOR(0 TO 0));

	end component;

	SIGNAL CLOCK		: STD_LOGIC;
	SIGNAL BUTTON_INT	: STD_LOGIC		:= '0';
	SIGNAL BTN			: STD_LOGIC		:= '0';
	SIGNAL LED_INT		: STD_LOGIC		:= '0';
	SIGNAL RECV_BIT		: STD_LOGIC		:= '0';
	SIGNAL RST1			: STD_LOGIC		:= '0';
	SIGNAL RST2			: STD_LOGIC		:= '0';
	SIGNAL SKIP_ROM		: STD_LOGIC		:= '0';
	SIGNAL SKIP_ROM1	: STD_LOGIC		:= '0';
	SIGNAL CNVRT		: STD_LOGIC		:= '0';
	SIGNAL RD_SCPD		: STD_LOGIC		:= '0';
	SIGNAL RD_ENTIRE	: STD_LOGIC		:= '0';
	SIGNAL MAX			: STD_LOGIC		:= '0';
	
	SIGNAL DONE_INT		: UNSIGNED(01 DOWNTO 0)		:= "00";
	SIGNAL COUNT_SEC	: UNSIGNED(12 DOWNTO 0)		:= (OTHERS=>'0');
	SIGNAL SEC_COUNT	: UNSIGNED(13 DOWNTO 0)		:= (OTHERS=>'0');
	SIGNAL COUNT		: UNSIGNED(07 DOWNTO 0)		:= (OTHERS=>'0');
	SIGNAL COMM_INT		: UNSIGNED(02 DOWNTO 0)		:= (OTHERS=>'0');
	SIGNAL SCRATCH_PAD	: UNSIGNED(71 DOWNTO 0)		:= (OTHERS=>'0');
	SIGNAL TO_DISP_INT	: UNSIGNED(07 DOWNTO 0)		:= (OTHERS=>'0');
	SIGNAL MAX_INT		: UNSIGNED(09 DOWNTO 0)		:= (OTHERS=>'0');
	SIGNAL MAX_COUNT	: UNSIGNED(25 DOWNTO 0)		:= (OTHERS=>'0');
	
	SIGNAL TURN			: UNSIGNED(07 DOWNTO 0)		:= TO_UNSIGNED(1, 8);
	SIGNAL CONTROL0		: STD_LOGIC_VECTOR(35 DOWNTO 0)		:= (OTHERS=>'0');
	SIGNAL TRIG0		: STD_LOGIC_VECTOR(00 DOWNTO 0)		:= (OTHERS=>'0');
	
	TYPE INSTRUCT	IS ARRAY	(0 TO 07)	OF UNSIGNED(02 DOWNTO 0);
	CONSTANT SKP_ROM	: INSTRUCT		:= ("010", "010", "001", "001", "010", "010", "001", "001");	-- CC HEX
	CONSTANT CONVERT	: INSTRUCT		:= ("010", "010", "001", "010", "010", "010", "001", "010");	-- 44 HEX
	CONSTANT SCPD_RD	: INSTRUCT		:= ("010", "001", "001", "001", "001", "001", "010", "001");	-- BE HEX
	CONSTANT READ_BIT	: UNSIGNED(02 DOWNTO 0)		:= "011";

BEGIN

	LED		<= LED_INT;
	Seven_Segment		<= TO_DISP_INT;

	PROCESS(CLOCK)
	BEGIN
	
		IF RISING_EDGE(CLOCK)	THEN
		
			BUTTON_INT		<= NOT BUTTON;
			
			TRIG0(0)		<= RST2;
			
			COMM_INT		<= (OTHERS=>'1');
			
			IF BTN='1'	THEN
				COMM_INT		<= "000";
				SKIP_ROM		<= '1';
				COUNT			<= (OTHERS=>'0');
			END IF;	-- BUTTON CLICK, SEND RESET PULS
			
			IF SKIP_ROM='1'		THEN
				FOR I	IN 0 TO 6	LOOP
					IF DONE_INT="10"	AND TURN(I)='1'	THEN
						TURN(I)			<= '0';
						TURN(I+1)		<= '1';
						COMM_INT		<= SKP_ROM(I);
					END IF;
				END LOOP;
				IF DONE_INT="10"	AND TURN(7)='1'	THEN
					SKIP_ROM		<= '0';
					CNVRT			<= '1';
					TURN(7)			<= '0';
					TURN(0)			<= '1';
					COMM_INT		<= SKP_ROM(7);
				END IF;
			END IF;	-- SKIP ROM: CC HEX
			
			IF CNVRT='1'	THEN
				FOR I	IN 0 TO 6	LOOP
					IF DONE_INT="10"	AND TURN(I)='1'	THEN
						TURN(I)		<= '0';
						TURN(I+1)	<= '1';
						COMM_INT	<= CONVERT(I);
					END IF;
				END LOOP;
				IF DONE_INT="10"	AND TURN(7)='1'	THEN
					CNVRT		<= '0';
					MAX			<= '1';
					TURN(7)		<= '0';
					TURN(0)		<= '1';
					COMM_INT	<= CONVERT(7);
				END IF;
			END IF;	-- CONVERT TEMPERATURE
			
			-- WAIT FOR CONVERT TEMPERATURE
			MAX_INT		<= MAX_COUNT(25) & MAX_COUNT(22) & MAX_COUNT(21) & MAX_COUNT(20) & MAX_COUNT(19) & 
							MAX_COUNT(14) & MAX_COUNT(13) & MAX_COUNT(11) & MAX_COUNT(7) & MAX_COUNT(6);
			IF MAX='1'	THEN
				MAX_COUNT		<= MAX_COUNT + 1;
				IF MAX_INT=("11" & X"FF")	THEN
					MAX			<= '0';
					RST1		<= '1';
				END IF;
			END IF;	-- WAIT FOR CONVERT TEMPERATURE
			
			IF RST1='1'	THEN
				IF DONE_INT="10"	THEN
					RST1			<= '0';
					COMM_INT		<= "000";
					SKIP_ROM1		<= '1';
				END IF;
			END IF;	-- SEND SECOND RESET
			
			IF SKIP_ROM1='1'		THEN
				FOR I	IN 0 TO 6	LOOP
					IF DONE_INT="10"	AND TURN(I)='1'	THEN
						TURN(I)			<= '0';
						TURN(I+1)		<= '1';
						COMM_INT		<= SKP_ROM(I);
					END IF;
				END LOOP;
				IF DONE_INT="10"	AND TURN(7)='1'	THEN
					SKIP_ROM1		<= '0';
					RD_SCPD			<= '1';	-- READ SCRATCH PAD
					TURN(7)			<= '0';
					TURN(0)			<= '1';
					COMM_INT		<= SKP_ROM(7);
				END IF;
			END IF;	-- SECOND SKIP ROM: CC HEX
			
			IF RD_SCPD='1'	THEN
				FOR I	IN 0 TO 6	LOOP
					IF DONE_INT="10"	AND TURN(I)='1'	THEN
						TURN(I)		<= '0';
						TURN(I+1)	<= '1';
						COMM_INT	<= SCPD_RD(I);
					END IF;
				END LOOP;
				IF DONE_INT="10"	AND TURN(7)='1'	THEN
					RD_SCPD		<= '0';
					RD_ENTIRE	<= '1';
					TURN(7)		<= '0';
					TURN(0)		<= '1';
					COMM_INT	<= SCPD_RD(7);
				END IF;
			END IF;	-- READ SCRATCH PAD
			
			IF RD_ENTIRE='1'	THEN
				IF DONE_INT="10"	THEN
					COUNT		<= COUNT + 1;
					COMM_INT	<= READ_BIT;
					FOR I	IN 1	TO 72	LOOP
						IF COUNT=TO_UNSIGNED(I, 7)	THEN
							SCRATCH_PAD(I-1)		<= RECV_BIT;
						END IF;
					END LOOP;
				END IF;
				IF COUNT=TO_UNSIGNED(72, 7)	THEN
					RD_ENTIRE		<= '0';
					RST2			<= '1';
				END IF;
			END IF;	-- READ ENTIRE SCRATCH PAD PLUS CRC
			
			IF RST2='1'	THEN
				IF DONE_INT="10"	THEN
					RST2			<= '0';
					COMM_INT		<= "000";
				END IF;
			END IF;	-- SEND THIRD RESET
			
			
			COUNT_SEC		<= COUNT_SEC + 1;
			IF COUNT_SEC=TO_UNSIGNED(6100, 13)	THEN
				SEC_COUNT		<= SEC_COUNT + 1;
				IF SEC_COUNT=TO_UNSIGNED(8000, 14)	THEN
					SEC_COUNT		<= (OTHERS=>'0');
					LED_INT			<= NOT LED_INT;
					TO_DISP_INT(7)	<= NOT LED_INT;
				END IF;
			END IF;	-- BLINK LED
			
			
			CASE SCRATCH_PAD(7 DOWNTO 4)	IS
				WHEN "0000"	=>
					TO_DISP_INT(6 DOWNTO 0)	<= "0111111"; -- 0
				WHEN "0001"	=>
					TO_DISP_INT(6 DOWNTO 0)	<= "0000110"; -- 1
				WHEN "0010"	=>
					TO_DISP_INT(6 DOWNTO 0)	<= "1011011"; -- 2
				WHEN "0011"	=>
					TO_DISP_INT(6 DOWNTO 0)	<= "1001111"; -- 3
				WHEN "0100"	=>
					TO_DISP_INT(6 DOWNTO 0)	<= "1100110"; -- 4
				WHEN "0101"	=>
					TO_DISP_INT(6 DOWNTO 0)	<= "1101101"; -- 5
				WHEN "0110"	=>
					TO_DISP_INT(6 DOWNTO 0)	<= "1111101"; -- 6
				WHEN "0111"	=>
					TO_DISP_INT(6 DOWNTO 0)	<= "0000111"; -- 7
				WHEN "1000"	=>
					TO_DISP_INT(6 DOWNTO 0)	<= "1111111"; -- 8
				WHEN "1001"	=>
					TO_DISP_INT(6 DOWNTO 0)	<= "1101111"; -- 9
				WHEN "1010"	=>
					TO_DISP_INT(6 DOWNTO 0)	<= "1110111"; -- A
				WHEN "1011"	=>
					TO_DISP_INT(6 DOWNTO 0)	<= "1111100"; -- B
				WHEN "1100"	=>
					TO_DISP_INT(6 DOWNTO 0)	<= "0111001"; -- C
				WHEN "1101"	=>
					TO_DISP_INT(6 DOWNTO 0)	<= "1011110"; -- D
				WHEN "1110"	=>
					TO_DISP_INT(6 DOWNTO 0)	<= "1111001"; -- E
				WHEN "1111"	=>
					TO_DISP_INT(6 DOWNTO 0)	<= "1110001"; -- F
				WHEN OTHERS =>
					TO_DISP_INT(6 DOWNTO 0)	<= "0000000"; -- DP
			END CASE;
		
		END IF;	-- RISING EDGE
	
	END PROCESS;
	
	INS_DEBOUNCE:	ENTITY WORK.DEBOUNCE(BEHAVIORAL)
	PORT MAP(
		CLOCK, '0', BUTTON_INT, OPEN, BTN
	);
	
	INS_THRMO:		ENTITY WORK.THERMO(BEHAVIORAL)
	PORT MAP(
		CLOCK, COMM_INT, TPTR, RECV_BIT, DONE_INT
	);
	
	CLOCK_INS : CLOCK_BUF
	port map
	(-- Clock in ports
	CLK_IN1 => CLOCK_IN,
	-- Clock out ports
	CLK_OUT1 => CLOCK);
	
	ICON_INS : ICON
	port map (
	CONTROL0 => CONTROL0);
	
	ILA_INS : ILA
	port map (
	CONTROL => CONTROL0,
	CLK => CLOCK,
	DATA => STD_LOGIC_VECTOR(SCRATCH_PAD),
	TRIG0 => TRIG0);

END BEHAVIORAL;